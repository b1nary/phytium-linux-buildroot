#!/bin/sh
echo "overlay prepare :mount fstab"
mount -a
echo "overlay prepare :make dev nodes"
mknod /dev/null c 1 3

mknod /dev/sda b 8 0
mknod /dev/sda1 b 8 1
mknod /dev/sda2 b 8 2

mknod /dev/loop0 b 7 0
mknod /dev/loop1 b 7 1

echo "overlay prepare :make overlay mount dirs"
mkdir -p /mnt/lower
mkdir -p /mnt/overlay
mkdir -p /mnt/newroot
mkdir -p /mnt/boot

echo "overlay prepare :load overlay module"
insmod /root/overlay.ko || echo "ERROR: insmod overlay failed"
echo "overlay prepare :load loop module"
insmod /root/loop.ko || echo "ERROR: insmod loop failed"

echo "overlay prepare :mount boot"
mount /dev/sda1 /mnt/boot || echo "mount lower failed"
echo "overlay prepare :mount overlay lower"
mount /mnt/boot/rootfs.ext2 /mnt/lower || echo "mount lower failed"
echo "overlay prepare :mount overlay upper"
mount /dev/sda2 /mnt/overlay || echo "mount upper failed"

mkdir -p /mnt/overlay/upper
mkdir -p /mnt/overlay/work

echo "overlay prepare :mount overlay fs"
mount -t overlay -o lowerdir=/mnt/lower,upperdir=/mnt/overlay/upper,workdir=/mnt/overlay/work overlayfs /mnt/newroot || \
    echo "ERROR: could not mount overlayFS"

echo "overlay prepare :change root and reinit"
exec switch_root -c /dev/console /mnt/newroot /sbin/init
