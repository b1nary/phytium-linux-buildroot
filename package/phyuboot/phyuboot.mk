################################################################################
#
# Phytium Pi uboot
#
################################################################################

PHYUBOOT_VERSION = 1.32-v2.1
PHYUBOOT_SITE = package/phyuboot/src
PHYUBOOT_SITE_METHOD = local
PHYUBOOT_DEPENDENCIES = linux host-dtc
MKIMAGE_PI = $(HOST_DIR)/bin/mkimage_phypi
# The only available license files are in PDF and RTF formats, and we
# support only plain text.

PHYUBOOT_INSTALL_IMAGES = YES
PHYUBOOT_RAMSIZE = $(BR2_PACKAGE_PHYUBOOT_RAMSIZE)

define PHYUBOOT_INSTALL_IMAGES_CMDS
	$(INSTALL) -D -m 0777 $(@D)/fip-all-$(BR2_PACKAGE_PHYUBOOT_RAMSIZE).bin $(BINARIES_DIR)/fip-all.bin
	$(INSTALL) -D -m 0777 $(@D)/kernel.its $(BINARIES_DIR)/kernel.its
 	$(INSTALL) -D -m 755 $(@D)/mkimage  $(HOST_DIR)/bin/mkimage_phypi
	$(MKIMAGE_PI) -f $(BINARIES_DIR)/kernel.its  $(BINARIES_DIR)/fitImage
endef

$(eval $(generic-package))
