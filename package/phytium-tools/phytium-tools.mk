################################################################################
#
# phytium-tools
#
################################################################################

PHYTIUM_TOOLS_VERSION = 0.1
PHYTIUM_TOOLS_SITE = package/phytium-tools/src
PHYTIUM_TOOLS_SITE_METHOD = local
PHYTIUM_TOOLS_INSTALL_TARGET_CMDS = YES

define  PHYTIUM_TOOLS_INSTALL_TARGET_CMDS
        mkdir -p $(TARGET_DIR)/usr/bin
	mkdir -p $(TARGET_DIR)/lib/firmware/rtlbt
	mkdir -p $(TARGET_DIR)/lib/systemd/system/
	$(INSTALL) -m 755 -D $(@D)/rtlbt/* $(TARGET_DIR)/lib/firmware/rtlbt/
	$(INSTALL) -m 755 -D $(@D)/rtk_hciattach $(TARGET_DIR)/usr/bin/
	$(INSTALL) -m 644 -D $(@D)/systemd-hciattach.service $(TARGET_DIR)/lib/systemd/system/
endef

$(eval $(generic-package))
